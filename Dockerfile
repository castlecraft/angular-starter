FROM node:latest
ARG STAGE=production
# Copy and build server
COPY . /opt/business-ui
WORKDIR /opt/
RUN cd business-ui \
    && cp src/environments/environment.example.ts src/environments/environment.ts \
    && yarn \
    && yarn build --configuration ${STAGE} \
    && yarn cap sync && yarn cap copy

FROM nginxinc/nginx-unprivileged:latest

COPY --from=0 /opt/business-ui/www /var/www/html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
