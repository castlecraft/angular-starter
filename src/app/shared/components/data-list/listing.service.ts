import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, share, switchMap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { TokenService } from '../../../auth/token/token.service';
import {
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
} from '../../../auth/token/constants';

export interface ListEvent {
  doctype?: string;
  filters?: string[][];
  fields?: string[];
}

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  private onSubject: Subject<ListEvent> = new Subject();
  public changes = this.onSubject.asObservable().pipe(share());

  constructor(
    private readonly http: HttpClient,
    private readonly token: TokenService,
  ) {}

  findModels(
    url: string,
    filters = {},
    sortOrder = undefined,
    pageNumber = 0,
    pageSize = 10,
    headers = {},
  ): Observable<any> {
    const offset = pageNumber * pageSize;
    let params = new HttpParams().set('offset', offset).set('limit', pageSize);
    if (sortOrder) {
      params = params.append('sort', sortOrder as string);
    }

    Object.keys(filters).forEach(key => {
      params = params.append(key, filters[key] as string);
    });

    return this.token.getToken().pipe(
      switchMap(token => {
        return this.http
          .get<{ docs: any[]; length: number; offset: number }>(url, {
            params,
            headers: {
              ...headers,
              [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
            },
          })
          .pipe(
            map(res => {
              return {
                docs: res.docs,
                offset: res.offset,
                length: res.length,
              };
            }),
          );
      }),
    );
  }

  getRequest(url: string, params?, headers?) {
    return this.token.getToken().pipe(
      switchMap(token => {
        if (typeof headers === 'object') {
          headers = { ...headers, Authorization: BEARER_HEADER_PREFIX + token };
        } else {
          headers = { Authorization: BEARER_HEADER_PREFIX + token };
        }
        return this.http.get<any>(url, { params, headers });
      }),
    );
  }

  postRequest(url: string, payload?, headers?) {
    return this.token.getToken().pipe(
      switchMap(token => {
        if (typeof headers === 'object') {
          headers = { ...headers, Authorization: BEARER_HEADER_PREFIX + token };
        } else {
          headers = { Authorization: BEARER_HEADER_PREFIX + token };
        }
        return this.http.post<any>(url, payload, { headers });
      }),
    );
  }
}
