import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { debounceTime, filter, map, startWith } from 'rxjs/operators';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
} from '../../../constants/strings';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent<T> implements OnInit {
  @Input() title = 'Confirm to Delete';
  @Input() message = 'Are you sure?';
  @Input() confirmText = 'Yes';
  @Input() cancelText = 'No';
  @Input() confirmationInput = '';
  confirmationControl = new FormControl('', [
    Validators.required,
    Validators.pattern(this.confirmationInput),
  ]);

  constructor(
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService,
    private dialogRef: MatDialogRef<T>,
  ) {}

  ngOnInit() {
    this.confirmationControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(300),
        filter(() => this.confirmationInput.length !== 0),
        map(val => {
          this.confirmationControl.setValidators([
            Validators.required,
            Validators.pattern(this.confirmationInput),
          ]);
          this.confirmationControl.updateValueAndValidity();
          return val;
        }),
      )
      .subscribe(value => {
        if (
          this.confirmationInput &&
          this.confirmationControl.value !== value
        ) {
          this.confirmationControl.setErrors(null);
        }
      });

    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
  }

  close() {
    this.dialogRef.close(false);
  }

  add() {
    this.dialogRef.close(true);
  }
}
