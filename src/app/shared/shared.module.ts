import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { TitleCaseFilterPipe } from '../pipes/title-case-filter.pipe';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { DataListComponent } from './components/data-list/data-list.component';

@NgModule({
  declarations: [
    DataListComponent,
    ConfirmationDialogComponent,
    TitleCaseFilterPipe,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
  ],
  exports: [DataListComponent, ConfirmationDialogComponent],
})
export class SharedModule {}
