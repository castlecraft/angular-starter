import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { SET_ITEM, StorageService } from './auth/storage/storage.service';
import { LOGGED_IN } from './auth/token/constants';
import { TokenService } from './auth/token/token.service';
import { DARK_MODE, ON, OFF, DARK_MODE_CLASS_NAME } from './constants/strings';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  loggedIn: boolean;
  cap = App;
  @HostBinding() class = '';
  toggleControl = new FormControl(false);
  isTenantAdmin = false;
  roles = [];
  tenantRoles = [];
  tenantName = '';
  tenantId = '';

  constructor(
    private breakpointObserver: BreakpointObserver,
    private readonly token: TokenService,
    private readonly store: StorageService,
    private readonly router: Router,
    private readonly zone: NgZone,
    private readonly overlay: OverlayContainer,
  ) {}

  ngOnInit() {
    this.initializeApp();
    if (this.loggedIn) {
      this.loadProfile();
    }

    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
      if (this.loggedIn) {
        this.loadProfile();
      }
    });

    this.store.getItem(DARK_MODE).then(darkMode => {
      this.setTheme(darkMode === ON ? true : false);
      this.toggleDarkMode(darkMode === ON ? true : false);
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (!this.loggedIn) {
            this.roles = [];
            this.router.navigate(['/home']).then(navigated => {});
          }
          if (this.loggedIn) {
            this.loadProfile();
          }
        }
      },
      error: error => {},
    });
  }

  setTheme(darkMode: boolean) {
    this.saveTheme(darkMode);
    if (darkMode) {
      this.class = DARK_MODE_CLASS_NAME;
      this.overlay.getContainerElement().classList.add(this.class);
    } else {
      this.class = '';
      this.overlay.getContainerElement().classList.remove(DARK_MODE_CLASS_NAME);
    }
  }

  toggleDarkMode(darkMode: boolean) {
    if (darkMode) {
      this.toggleControl.setValue(true);
    } else {
      this.toggleControl.setValue(false);
    }
  }

  saveTheme(darkMode: boolean) {
    if (darkMode) {
      this.store.setItem(DARK_MODE, ON);
    } else {
      this.store.setItem(DARK_MODE, OFF);
    }
  }

  initializeApp() {
    this.cap.addListener('appUrlOpen', (data: { url?: string }) => {
      this.zone.run(() => {
        const slug = data.url
          ?.split(`${environment.callbackProtocol}://`)
          .pop();
        if (slug) {
          this.router.navigateByUrl(slug);
        }
      });
    });
  }

  logIn() {
    this.token.logIn();
  }

  chooseAccount() {
    this.token.logOut().then(() => this.token.logIn());
  }

  logOut() {
    this.token.logOut();
  }

  loadProfile() {
    this.token.loadProfile().subscribe({
      next: profile => {
        this.roles = profile?.roles;
      },
      error: error => {},
    });
  }
}
