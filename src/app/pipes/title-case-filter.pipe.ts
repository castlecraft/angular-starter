import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleCaseFilter',
})
export class TitleCaseFilterPipe implements PipeTransform {
  transform(value: string): string {
    const text = value.replace(/([A-Z])/g, ' $1').toLowerCase();
    const finalText = text.charAt(0).toUpperCase() + text.slice(1);
    return finalText;
  }
}
