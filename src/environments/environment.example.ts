// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // Configure Variables
  authorizationURL: 'https://staging-accounts.example.com/oauth2/confirmation',
  authServerURL: 'https://staging-accounts.example.com',
  callbackProtocol: 'blocks',
  callbackURL: 'http://apps.localhost:4200/callback',
  clientId: '2e364449-c149-4cf2-be13-9634621b9f27',
  profileURL: 'https://staging-accounts.example.com/oauth2/profile',
  revocationURL:
    'https://myaccount.example.com/connected_device/revoke_token',
  scope: 'profile openid email roles phone',
  tokenURL: 'https://staging-accounts.example.com/oauth2/token',
  isAuthRequiredToRevoke: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
