export const environment = {
  production: true,

  // Configure Variables
  authorizationURL: 'https://accounts.example.com/oauth2/confirmation',
  authServerURL: 'https://accounts.example.com',
  callbackProtocol: 'blocks',
  callbackURL: 'http://apps.example.com/callback',
  clientId: '2e364449-c149-4cf2-be13-9634621b9f27',
  profileURL: 'https://accounts.example.com/oauth2/profile',
  revocationURL:
    'https://myaccount.example.com/connected_device/revoke_token',
  scope: 'profile openid email roles phone',
  tokenURL: 'https://accounts.example.com/oauth2/token',
  isAuthRequiredToRevoke: true,
};

// client_id is available for public, client_secret is to be protected.
